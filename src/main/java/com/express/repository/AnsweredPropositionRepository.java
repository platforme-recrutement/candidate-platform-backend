package com.express.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.express.model.AnsweredProposition;

@Repository
public interface AnsweredPropositionRepository extends JpaRepository<AnsweredProposition, Long> {
}