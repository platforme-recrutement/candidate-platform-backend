package com.express.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.express.model.PassedQuestion;

@Repository
public interface PassedQuestionRepository extends JpaRepository<PassedQuestion, Long> {

}