package com.express.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.express.model.OnlineInterview;

@Repository
public interface OnlineInterviewRepository extends JpaRepository<OnlineInterview, Long> {

}
