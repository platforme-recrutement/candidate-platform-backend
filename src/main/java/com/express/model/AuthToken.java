package com.express.model;

public class AuthToken {

	private String token;

	public AuthToken() {
	}

	/**
	 * Constructs token of authenticated user
	 * 
	 * @param token the token of authenticated user
	 */
	public AuthToken(String token) {
		this.token = token;
	}

	/**
	 * Gets the token of this authenticated user
	 * 
	 * @return
	 */
	public String getToken() {
		return token;
	}

	/**
	 * Changes the token of this authenticated user
	 * 
	 * @param token
	 */
	public void setToken(String token) {
		this.token = token;
	}

}
