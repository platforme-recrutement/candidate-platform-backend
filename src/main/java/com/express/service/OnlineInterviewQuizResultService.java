package com.express.service;

import java.util.List;

import com.express.model.OnlineInterviewQuizResult;
import com.express.model.OnlineInterviewQuizResultId;

public interface OnlineInterviewQuizResultService {

	OnlineInterviewQuizResult findById(OnlineInterviewQuizResultId onlineInterviewQuizResultId);

	List<OnlineInterviewQuizResult> findAll();

	OnlineInterviewQuizResult save(OnlineInterviewQuizResult onlineInterviewQuizResult);

	OnlineInterviewQuizResult update(OnlineInterviewQuizResultId id,
			OnlineInterviewQuizResult onlineInterviewQuizResult);

}
