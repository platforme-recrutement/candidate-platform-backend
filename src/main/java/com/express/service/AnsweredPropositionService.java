package com.express.service;

import com.express.model.AnsweredProposition;


public interface AnsweredPropositionService {
	AnsweredProposition create(AnsweredProposition answeredProposition);

	AnsweredProposition update(Long id, AnsweredProposition answeredProposition);
}
