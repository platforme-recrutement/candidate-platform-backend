package com.express.service;

import java.util.List;

import com.express.model.Application;

public interface ApplicationService {

	Application findById(Long id);

	List<Application> findByStatus(String status);

}
