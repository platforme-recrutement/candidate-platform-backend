package com.express.service;

import com.express.model.Reclamation;

public interface ReclamationService {

	Reclamation create(Reclamation reclamation);

}