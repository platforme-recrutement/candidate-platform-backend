package com.express.service;

import com.express.model.PassedQuestion;

public interface PassedQuestionService {

	PassedQuestion create(PassedQuestion passedQuestion);

	PassedQuestion update(Long id, PassedQuestion passedQuestion);

}
