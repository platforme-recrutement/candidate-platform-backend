package com.express.service;

import com.express.model.Quiz;

public interface QuizService {

	Quiz findById(Long id);

}